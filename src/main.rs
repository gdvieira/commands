use std::slice;

//
// Este é um esboço de assinatura do registro de operações no disco.
// Observe que o registro guarda estruturas de qualquer tipo único.
//
struct VolatileLog<T> {
    actions: Vec<T>,
}

impl<T> VolatileLog<T> {
    fn new() -> Self {
        Self { actions: vec![] }
    }

    fn record(&mut self, action: T) {
        self.actions.push(action);
    }

    fn replay_as_iterator(&self) -> slice::Iter<T> {
        self.actions.iter()
    }
}

//
// Esta estrutura e seus métodos representam o estado que quero guardar
// indiretamente através do registro de ações. Coloquei uma coisa qualquer
// para ter o que rodar. Isso não é importante.
//
struct Account {
    balance: f64,
}

impl Account {
    fn new() -> Self {
        Self { balance: 0.0 }
    }

    fn balance(&self) -> f64 {
        self.balance
    }

    fn deposit(&mut self, amount: f64) {
        self.balance += amount;
    }

    fn withdraw(&mut self, amount: f64) {
        self.balance -= amount;
    }

    fn reset(&mut self) {
        self.balance = 0.0;
    }
}

//
// Esta é a característica dos tipos que representam ações, que usa
// um tipo associado para indicar onde as ações serão aplicadas.
//
trait Action {
    type Target; // Advanced Rust!!! (Cap 19.2)

    fn execute(&self, target: &mut Self::Target);
}

//
// Este é o tipo que eu quero efetivamente escrever no disco
// e ele deve implementar a característica Action com o tipo
// associado Account.
//
enum AccountAction {
    Deposit { amount: f64 },
    Withdraw { amount: f64 },
    Reset,
}

impl Action for AccountAction {
    type Target = Account;

    fn execute(&self, target: &mut Account) {
        match self {
            AccountAction::Deposit { amount } => target.deposit(*amount),
            AccountAction::Withdraw { amount } => target.withdraw(*amount),
            AccountAction::Reset => target.reset(),
        }
    }
}

//
// Um programinha bobo para amarrar todo mundo.
//
fn main() {
    // O estado
    let mut acct = Account::new();

    // As ações
    let mut actions = VolatileLog::new();
    actions.record(AccountAction::Deposit { amount: 5.0 });
    actions.record(AccountAction::Deposit { amount: 4.0 });
    actions.record(AccountAction::Withdraw { amount: 7.0 });
    actions.record(AccountAction::Reset);

    // (Re)Executando as ações
    for action in actions.replay_as_iterator() {
        action.execute(&mut acct);
        println!("{}", acct.balance());
    }
 }
 